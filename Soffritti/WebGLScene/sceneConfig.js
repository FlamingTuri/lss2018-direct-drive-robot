const config = {
    floor: {
        size: { x: 20, y: 20 }
    },
    player: {
        //position: { x: 0.5, y: 0.5 },	  //CENTER
        position: { x: 0.2, y: 0.16 },	//INIT
        //position: { x: 0.8, y: 0.85 },  //END
        speed: 0.2
    },
    sonars: [/*
        {
            name: "sonar1",
            position: { x: 0.12, y: 0.05 },
            senseAxis: { x: false, y: true }
        },
        {
            name: "sonar2",
            position: { x: 0.94, y: 0.88 },
            senseAxis: { x: true, y: false }
        }*/
    ],
    movingObstacles: [/*
        {
            name: "movingobstacle",
            position: { x: .64, y: .42 },
            directionAxis: { x: true, y: true },
            speed: 0.2,
            range: 28
        },
        {
            name: "wall",
            position: { x: 0.0, y: 0.6 },
            directionAxis: { x: true, y: false },
            speed: 0.0078,
            range: 120
        }*/

    ],
    staticObstacles: [
        {
            name: "bag1",
            centerPosition: { x: 0.4, y: 0.38 },
            size: { x: 0.10, y: 0.045 }
        },
        {
            name: "bag2",
            centerPosition: { x: 0.75, y: 0.66 },
            size: { x: 0.05, y: 0.045 }
        },
        {
            name: "bagWithBomb",
            centerPosition: { x: 0.70, y: 0.2 },
            size: { x: 0.05, y: 0.045 }
        },
        {
            name: "wallUp",
            centerPosition: { x: 0.5, y: 1.00 },
            size: { x: 0.90, y: 0.01 }
        },
        {
            name: "wallDown",
            centerPosition: { x: 0.5, y: 0.05 },
            size: { x: 0.90, y: 0.01 }
        },
        {
            name: "wallRight",
            centerPosition: { x: 0.05, y: 0.5 },
            size: { x: 0.01, y: 0.8 }
        },
        {
            name: "wallLeft",
            centerPosition: { x: 0.95, y: 0.5 },
            size: { x: 0.01, y: 0.8 }
        }
    ]
}

export default config;
