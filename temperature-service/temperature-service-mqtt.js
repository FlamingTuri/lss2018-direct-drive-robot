const mqtt = require('mqtt');;
const constants = require("./constants");
var client = mqtt.connect('mqtt://iot.eclipse.org');
var currentTemperature = "25";

client.on('connect', () => {
    client.subscribe(constants.requestTemperatureTopic);
});

// The message usually arrives as buffer, so we convert it to string data type;
client.on('message', (topic, message) => {
    switch (topic) {
        case constants.requestTemperatureTopic:
            client.publish(constants.responseTemperatureTopic, qactorFormattedTemperature(currentTemperature));
            break;
        default:
            console.log("not implemented");
    }
    /*var msg = message.toString();
    console.log("\t MQTT RECEIVES:" + msg); //if toString is not given, the message comes as buffer;
    if (msg.indexOf("sonarDetect") > 0) {
        channel.emit("sonarDetect", msg);  //to allow update of the WebPage
    } else if (msg.indexOf("sonar") > 0) {
        channel.emit("sonar", msg);  //to allow update of the WebPage
    }*/
});

function notifyTemperatureChange(msg) {
    client.publish(constants.hallTopic, qactorFormattedTemperature(msg));
}

function qactorFormattedTemperature(value) {
    return qactorFormattedMsg("temperature", "event", "js", "none", "temperature( " + value + ")", 1);
}

/**
 * 
 * @param {*} msgID Message identifier
 * @param {*} msgType Message type (e.g.: dispatch,request,event)
 * @param {*} sender Identifier of the sender
 * @param {*} receiver Identifier of the receiver
 * @param {*} content Message payload
 * @param {*} sequnum Unique natural number associated to the message
 */
function qactorFormattedMsg(msgID, msgType, sender, receiver, content, sequnum) {
    return "msg(" + msgID + "," + msgType + "," + sender + "," + receiver + "," + content + "," + sequnum + ")";
}


// mock temperature generator
var stdin = process.openStdin();
var stdout = process.stdout;
var msg = "insert current temperature: ";

stdin.addListener("data", (d) => {
    // note: d is an object, and when converted to a string it will
    // end with a linefeed. So we (rather crudely) account for that  
    // with toString() and then trim()
    var temperature = d.toString().trim();

    if (currentTemperature != temperature) {
        currentTemperature = temperature;
        // console.log(currentTemperature);
        //notifyTemperatureChange(temperature);
        notifyTemperatureChange(temperature);
    }

    stdout.write("\n" + msg);
});

stdout.write(msg);
