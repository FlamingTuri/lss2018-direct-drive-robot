const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://localhost')
const constants = require('./constants');
var temperature;

client.on('connect', () => {
    client.subscribe(constants.responseTemperatureTopic);
});

client.on('message', (topic, message) => {
    switch (topic) {
        case constants.hallTopic:
            console.log(message);
            temperature = message.toString();
            console.log("broadcast received temperature: " + temperature);
            break;
        case constants.responseTemperatureTopic:
            client.unsubscribe(constants.responseTemperatureTopic);
            client.subscribe(constants.hallTopic);
            temperature = message.toString();
            console.log("requested temperature: " + temperature);
            break;
    }
});

function requestTemperature() {
    console.log("requesting temperature");
    client.publish(constants.requestTemperatureTopic, "");
};

requestTemperature();
setTimeout(requestTemperature, 3000);
