let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let port = 3333;
let currentTemperature = 24;

let welcomeEvent = "welcomeTemperature";
let broadcastEvent = "temperature"

// creating WebSocket for chat message exchange
io.on('connection', (socket) => {
  console.log('user connected');
  io.emit(welcomeEvent, currentTemperature);

  // log user disconnect activity
  socket.on('disconnect', function () {
    console.log('user disconnected');
  });

  // distribute incoming messages to chat users
  /*socket.on('add-message', (message) => {
      console.log('received message: ');
      console.log(message);

      let date = new Date().toJSON().slice(0, 19).replace('T', ' ');
      console.log(date);

      io.emit('message', message);
  });*/

});

/*
function sendTemperature() {
  console.log("sending temperature");
  io.emit(welcomeEvent, "temperature(2500)");
}*/

function notifyTemperatureChange(temperature) {
  console.log("emitted " + temperature);
  io.emit(broadcastEvent, temperature);
}

/*
setTimeout(sendTemperature, 10000);

setTimeout(notifyTemperatureChange, 13000);
setTimeout(notifyTemperatureChange, 15000);
*/

http.listen(port, function () {
  console.log('listening on *:' + port);

  // mock temperature generator
  let stdin = process.openStdin();
  let stdout = process.stdout;
  let msg = "insert current temperature: ";

  stdin.addListener("data", (d) => {
    // note: d is an object, and when converted to a string it will
    // end with a linefeed. So we (rather crudely) account for that  
    // with toString() and then trim()
    let temperature = d.toString().trim();

    if (currentTemperature != temperature) {
      currentTemperature = temperature;
      // console.log(currentTemperature);
      notifyTemperatureChange(temperature);
    }

    stdout.write("\n" + msg);
  });

  stdout.write(msg);

});
