#!/bin/bash

# cd to the current script directoy
cd "$(dirname "$0")"

cd 'discovery-robot'; ./gradlew eclipse
cd '../Soffritti'; ./install.sh
cd '../robotFrontend'; ./install.sh
cd '../temperature-service'; ./install.sh
