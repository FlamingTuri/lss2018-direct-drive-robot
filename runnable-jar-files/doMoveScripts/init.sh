#!/bin/bash
in1=22 #WPI 3 BCM 22  PHYSICAL 15
in2=23 #WPI 4 BCM 23  PHYSICAL 16

in1B=10 #WPI 12  BCM 10 PHYSICAL 19
in2B=9  #WPI 13  BCM 9  PHYSICAL 21  
 

echo "creating cwA gpio${in1}"
gpio export ${in1} out

echo "creating ccwA gpio${in1}"
gpio export ${in2} out

echo "creating cwB gpio${in1}"
gpio export ${in1B} out

echo "creating ccwB gpio${in1}"
gpio export ${in2B} out
