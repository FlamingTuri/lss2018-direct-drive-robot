#!/bin/bash

GPIOPATH="/sys/class/gpio/gpio"

# WPI 8  BCM 2  PHYSICAL 3
# WPI 9  BCM 3  PHYSICAL 5

# Broadcom SOC channel pin
ledPin=2
# Wiring PI pin
ledPinwp=8

if [ -d ${GPIOPATH}${ledPin} ]
then
  echo "in1 gpio${ledPin} exist"
else
  echo "creating in1 gpio${ledPin}"
fi
gpio export ${ledPin} out

if [ $1 = "true" ];
then
  $ON=1
else 
  $ON=0
fi
gpio write ${ledPinwp} $ON
#echo "led $1"
