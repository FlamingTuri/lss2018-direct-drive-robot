#!/bin/bash
# -----------------------------------------------------------------
# nanoMotorDrive.sh
# test for nano0
# Key-point: we can manage a GPIO pin  by using the GPIO library.
# On a OC, edit this file as UNIX
# -----------------------------------------------------------------

# WPI 12  BCM 10  PHYSICAL 19
# WPI 13  BCM 9   PHYSICAL 21

# Broadcom SOC channel pin
in1=10
in2=9
# Wiring PI pin
inwp1=12
inwp2=13

if [ -d /sys/class/gpio/gpio${in1} ]
then
  echo "in1 gpio${in1} exist"
else
  echo "creating in1 gpio${in1}"
fi
gpio export ${in1} out

if [ -d /sys/class/gpio/gpio${in2} ]
then
  echo "in2 gpio${in2} exist"
else
  echo "creating in2  gpio${in2}"
fi
gpio export ${in2} out

gpio readall

echo "run 1"
gpio write ${inwp1} 0
gpio write ${inwp2} 1
sleep 1.5

echo "run 2"
gpio write ${inwp1} 1
gpio write ${inwp2} 0
sleep 1.5

echo "stop"
gpio write ${inwp1} 0
gpio write ${inwp2} 0

gpio readall
