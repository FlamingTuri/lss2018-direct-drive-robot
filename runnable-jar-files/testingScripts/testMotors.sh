#!/bin/bash

GPIOPATH="/sys/class/gpio/gpio"

###############
# motorDriveA #
###############

# WPI 8  BCM 2  PHYSICAL 3
# WPI 9  BCM 3  PHYSICAL 5

# Broadcom SOC channel pin
in1A=2
in2A=3
# Wiring PI pin
inwp1A=8
inwp2A=9

if [ -d ${GPIOPATH}${in1A} ]
then
  echo "in1 gpio${in1A} exist"
else
  echo "creating in1 gpio${in1A}"
fi
gpio export ${in1A} out

if [ -d ${GPIOPATH}${in2A} ]
then
  echo "in2 gpio${in2A} exist"
else
  echo "creating in2  gpio${in2A}"
fi
gpio export ${in2A} out


###############
# motorDriveB #
###############

# WPI 12  BCM 10  PHYSICAL 19
# WPI 13  BCM 9   PHYSICAL 21

# Broadcom SOC channel pin
in1B=10
in2B=9
# Wiring PI pin
inwp1B=12
inwp2B=13

if [ -d ${GPIOPATH}${in1B} ]
then
  echo "in1 gpio${in1B} exist"
else
  echo "creating in1 gpio${in1B}"
fi
gpio export ${in1B} out

if [ -d ${GPIOPATH}${in2B} ]
then
  echo "in2 gpio${in2B} exist"
else
  echo "creating in2  gpio${in2B}"
fi
gpio export ${in2B} out


###########
# testing #
###########

gpio readall

# testingA
echo "run A 1"
gpio write ${inwp1A} 0
gpio write ${inwp2A} 1
sleep 1.5

echo "run A 2"
gpio write ${inwp1A} 1
gpio write ${inwp2A} 0
sleep 1.5

echo "stop A"
gpio write ${inwp1A} 0
gpio write ${inwp2A} 0

# testingB
echo "run B 1"
gpio write ${inwp1B} 0
gpio write ${inwp2B} 1
sleep 1.5

echo "run B 2"
gpio write ${inwp1B} 1
gpio write ${inwp2B} 0
sleep 1.5

echo "stop B"
gpio write ${inwp1B} 0
gpio write ${inwp2B} 0


# testing rotation
echo "rotation 1"
gpio write ${inwp1A} 0
gpio write ${inwp2A} 1
gpio write ${inwp1B} 1
gpio write ${inwp2B} 0
sleep 1.5

echo "rotation 2"
gpio write ${inwp1A} 1
gpio write ${inwp2A} 0
gpio write ${inwp1B} 0
gpio write ${inwp2B} 1
sleep 1.5

echo "stop rotation"
gpio write ${inwp1A} 0
gpio write ${inwp2A} 0
gpio write ${inwp1B} 0
gpio write ${inwp2B} 0


gpio readall
