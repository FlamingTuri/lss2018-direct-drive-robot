#include <iostream>
#include <wiringPi.h>
#include <fstream>
#include <cmath>
using namespace std;


#define TRIG 0
#define ECHO 2

#define CLOSE 18
#define MEDIUM 21
#define FAR 60

#define POS_LEFT 0.055
#define POS_RIGHT 0.24
#define POS_FORWARD 0.14

/*
 * sudo java -jar ServoSonarMain.jar
 * g++ SonarAlone.cpp -l wiringPi -o SonarAlone
 */
void setup() {
    cout << "setUp " << endl;
    wiringPiSetup();
    pinMode(TRIG, OUTPUT);
    pinMode(ECHO, INPUT);
    //TRIG pin must start LOW
    digitalWrite(TRIG, LOW);
    delay(50);
}

int getDistance() {
    //Send trig pulse
    digitalWrite(TRIG, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG, LOW);

    //Wait for echo start
    while(digitalRead(ECHO) == LOW);

    //Wait for echo end
    long startTime = micros();
    while(digitalRead(ECHO) == HIGH);
    long travelTime = micros() - startTime;

    //Get distance in cm
    int distance = travelTime / 58;

    return distance;
}

int main() {
    int distance;
    setup();
    while(1) {
        distance = getDistance();
        cout << "distance(" << distance << ",forward,front)" << endl;
        delay(50);
    }
    return 0;
}
