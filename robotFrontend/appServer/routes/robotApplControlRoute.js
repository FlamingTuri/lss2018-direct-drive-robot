/*
 * appServer/routes/robotApplControlRoute.js
 */
var express = require('express');
var router = express.Router();

const robotControl = require('../controllers/applRobotControl');

router.post("/explore", function (req, res, next) {
	robotControl.actuate("explore", req, res);
	next();
});
router.post("/halt", function (req, res, next) {
	robotControl.actuate("halt", req, res);
	next();
});
router.post("/home", function (req, res, next) {
	robotControl.actuate("home", req, res);
	next();
});
router.post("/bombFound", function (req, res, next) {
	robotControl.actuate("bombFound", req, res);
	next();
});
router.post("/bagClean", function (req, res, next) {
	robotControl.actuate("bagClean", req, res);
	next();
});

module.exports = router;
