#!/bin/bash

# cd to the current script directoy
cd "$(dirname "$0")"

unameOut="$(uname -s)"

exec_in_a_new_terminal () {
    case "${unameOut}" in
        Linux*)     machine=Linux
            if [ -f $(which gnome-terminal) ]
            then
                gnome-terminal -- bash -c "$1"
            else
                x-terminal-emulator -e "$1"
            fi
            ;;
        Darwin*)    machine=Mac
            open -a Terminal "$1"
            # not working, the new opened terminal needs to cd to the script directory
            # osascript -e 'tell application "Terminal" to do script "cd path; ./start-server.sh"'
            ;;
        #CYGWIN*)    machine=Cygwin;;
        #MINGW*)     machine=MinGw;;
        *)          machine="UNKNOWN:${unameOut}"
    esac
}

exec_in_a_new_terminal 'cd Soffritti; ./startServer.sh; exec bash'
exec_in_a_new_terminal 'cd robotFrontend; ./startFrontend.sh; exec bash'
exec_in_a_new_terminal 'cd temperature-service; ./start-service.sh; exec bash'
