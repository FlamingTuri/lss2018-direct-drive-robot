# Bomb Detector Direct Drive Robot

Project developed for the course Laboratorio di Sistemi Software -- academic year 2018/2019


### Prerequisites

- Eclipse EE IDE
- xtext plugin for Eclipse 
- Java 8
- node & npm


### Installing

1. Download *eclipse-dropins.tar.gz* from the download section and put its content under the **dropins** directory inside your Eclipse installation folder
2. Import *discovery-robot project* in Eclipse
3. run `./setup.sh` or `setup.bat` (based on you OS)
4. remove duplicate entries (if any) from project build path: Right click on project -> Build Path -> Configure Build Path


### Running

1. run `./star-all.sh` or `star-all.bat` (based on you OS)
2. go to http://localhost:3000/ to open the commands gui 
3. go to http://localhost:8090/ to open the virtual environment
4. run *it.unibo.ctxRobotRetriever.MainCtxRobotRetriever*
5. run *it.unibo.ctxRobotExplorer.MainCtxRobotExplorer*


### Using a raspberry as a real robot

1. change the *Context*'s host inside [ddrRobot.qa](discovery-robot/src/ddrRobot.qa) with the local ip address of your computer and your robot
2. create jar files for *it.unibo.ctxRobotRetriever.MainCtxRobotRetriever* and  *it.unibo.ctxRobotExplorer.MainCtxRobotExplorer*
3. put them under **runnable-jar-files directory**
4. move **runnable-jar-files directory** to your raspberry
5. run `./compile.sh` to compile the sonar
6. launch *MainCtxRobotRetriever.jar* on the robot with `java -jar MainCtxRobotRetriever.jar`
7. launch *MainCtxRobotExplorer.jar* on your computer with `java -jar MainCtxRobotExplorer.jar`

---

For more infos about the hardware used and how to connect it to the raspberry see the file [robotConfig.md](robotConfig.md)

---

## Authors

- Placuzzi Andrea
- Venturini Giacomo


## License

This project is licensed under the GNU License - see the [LICENSE](LICENSE) file for details
