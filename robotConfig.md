Robot components:

- Raspberry PI 3 Model B
- H bridge: Dual H Bridge DC Stepper Motor Drive Controller Board Module Arduino L298N #236
- Sonar: HC-SR04 Distance Sensor
- Motors & wheels: TT motor TT wheel robot car motor + wheel suit

Connection raspi-Sonar:

- trig -> physical pin 11
- echo -> physical pin 13
	
Connection raspi-(H bridge)-right motor:

- cw   -> physical pin 15
- ccw  -> physical pin 16
	
Connection raspi-(H bridge)-left motor:

- cw   -> physical pin 19
- ccw  -> physical pin 21
	
Don't forget power connection!!!

---

To change the motors pin edit the scripts inside [runnable-jar-files/doMoveScripts](runnable-jar-files/doMoveScripts) folder

To change the sonar pin edit [SonarAlone.cpp](runnable-jar-files/SonarAlone.cpp) file
