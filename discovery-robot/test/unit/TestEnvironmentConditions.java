package unit;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import ddr.project.shared.Constants;
import ddr.project.shared.QActorMessageSender;
import ddr.project.shared.QActorsConstants;
import it.unibo.ddrenvobserverexplorer.Ddrenvobserverexplorer;
import it.unibo.ddrmindexplorer.Ddrmindexplorer;
import it.unibo.tester.Tester;

public class TestEnvironmentConditions extends TestingUtils {

    private Ddrenvobserverexplorer explorerEnvObserver;
    private Ddrmindexplorer explorerMind;
    private Tester tester;
    private final QActorMessageSender qaMessageSender = new QActorMessageSender();
    private final String temperatureMessageId = "temperature";

    private final String temperatureMessageContent(int temperature) {
        return String.format("temperature(%s)", temperature);
    }

    @Before
    public void setUp() {
        System.out.println("================== setUp ==================");
        new Thread(() -> {
            try {
                it.unibo.ctxRobotRetriever.MainCtxRobotRetriever.initTheContext();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        try {
            it.unibo.ctxRobotExplorer.MainCtxRobotExplorer.initTheContext();
        } catch (Exception e) {
            e.printStackTrace();
        }

        explorerEnvObserver = getInitializedQaReference(QActorsConstants.explorerEnvObserver);
        explorerMind = getInitializedQaReference(QActorsConstants.explorerMind);
        tester = getInitializedQaReference(QActorsConstants.tester);
    }

    @Test
    public void testEnvCondsChange() {
        // waitQactorsInitialization(envObserver, mind, tester);

        Prolog mindEngine = explorerMind.getPrologEngine();
        SolveInfo solveInfo;
        try {
            solveInfo = mindEngine.solve("envCond(X).");
            checkResult(solveInfo, "f", "X");

            int newTemperature = 23;
            qaMessageSender.selfMessage(explorerEnvObserver, Constants.DISPATCH_MSG, temperatureMessageId,
                    temperatureMessageContent(newTemperature));

            // waiting values update
            waitGoalUntil(tester, "testEnvironment(X)", "X", "true");

            solveInfo = mindEngine.solve("envCond(X).");
            checkResult(solveInfo, "t", "X");
        } catch (MalformedGoalException e) {
            e.printStackTrace();
        } catch (NoSolutionException e) {
            e.printStackTrace();
        }
    }

}
