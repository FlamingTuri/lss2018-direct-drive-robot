package unit;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import ddr.project.shared.Constants;
import ddr.project.shared.QActorMessageSender;
import ddr.project.shared.QActorsConstants;
import it.unibo.ddrenvobserverexplorer.Ddrenvobserverexplorer;
import it.unibo.tester.Tester;

public class TestTemperatureUpdate extends TestingUtils {

    private Ddrenvobserverexplorer explorerEnvObserver;
    private Tester tester;
    private final QActorMessageSender qaMessageSender = new QActorMessageSender();
    private final String temperatureMessageId = "temperature";

    private final String temperatureMessageContent(int temperature) {
        return String.format("temperature(%s)", temperature);
    }

    @Before
    public void setUp() {
        System.out.println("================== setUp ==================");
        new Thread(() -> {
            try {
                it.unibo.ctxRobotRetriever.MainCtxRobotRetriever.initTheContext();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        try {
            it.unibo.ctxRobotExplorer.MainCtxRobotExplorer.initTheContext();
        } catch (Exception e) {
            e.printStackTrace();
        }

        explorerEnvObserver = getInitializedQaReference(QActorsConstants.explorerEnvObserver);
        tester = getInitializedQaReference(QActorsConstants.tester);
    }

    @Test
    public void testTemperatureUpdate() {
        Prolog envObserverEngine = explorerEnvObserver.getPrologEngine();
        SolveInfo solveInfo;
        try {
            solveInfo = envObserverEngine.solve("maxTemp(X).");
            checkResult(solveInfo, "25", "X");

            solveInfo = envObserverEngine.solve("temp(X).");
            checkResult(solveInfo, "undefined", "X");

            int newTemperature = 23;
            qaMessageSender.selfMessage(explorerEnvObserver, Constants.DISPATCH_MSG, temperatureMessageId,
                    temperatureMessageContent(newTemperature));

            // waiting value update
            waitGoalUntil(tester, "testTemperature(X)", "X", "true");

            solveInfo = envObserverEngine.solve("temp(X).");
            checkResult(solveInfo, "" + newTemperature, "X");
        } catch (MalformedGoalException e) {
            e.printStackTrace();
        } catch (NoSolutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
