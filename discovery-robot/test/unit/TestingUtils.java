package unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import alice.tuprolog.NoSolutionException;
import alice.tuprolog.SolveInfo;
import ddr.project.shared.Utils;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.akka.QActor;

public class TestingUtils {

    /**
     * Waits until every qactor is not null
     *
     * @param qactors the qactors to check
     */
    public void waitQactorsInitialization(QActor... qactors) {
        Arrays.asList(qactors).forEach(qa -> {
            // System.out.println(Arrays.asList(qactors).indexOf(qa));
            while (qa == null) {
                System.out.print(".");
                Utils.sleep(500);
            }
            Utils.log("QA " + qa + " ready");
        });
    }

    /**
     * Shortcut for QActorUtils.getQActorForTesting(name);
     * 
     * @param qactorName
     * @return
     */
    public QActor getQActorForTesting(String qactorName) {
        return QActorUtils.getQActorForTesting(qactorName);
    }

    /**
     * Asserts that the solveInfo is successful, then asserts that the varValue of the solveInfo is
     * equal to the expectd value
     *
     * @param solveInfo
     * @param expected
     * @param varValue
     * @throws NoSolutionException
     */
    public void checkResult(SolveInfo solveInfo, String expected, String varValue) throws NoSolutionException {
        assertTrue(solveInfo.isSuccess());
        assertEquals(expected, solveInfo.getVarValue(varValue).toString());
    }

    /**
     * Gets the qactor reference waiting its initialization
     *
     * @param qaName the qactor name
     * @return the qactor reference
     */
    public <T extends QActor> T getInitializedQaReference(String qaName) {
        T qa = null;
        while (qa == null) {
            Utils.sleep(100);
            qa = getQaReference(qaName);
        }
        return qa;
    }

    /**
     * Gets the qactor reference. Warning: if the context is not completely initialized it will return
     * null
     *
     * @param qaName the qactor name
     * @return the qactor reference
     */
    @SuppressWarnings("unchecked")
    public <T extends QActor> T getQaReference(String qaName) {
        T qaReference = null;
        T qa = (T) getQActorForTesting(qaName);
        try {
            while (!qa.solveGoal("ready(X)").getVarValue("X").toString().equals("true")) {
                Utils.sleep(100);
                qa = (T) getQActorForTesting(qaName);
            }
            qaReference = qa;
        } catch (NoSolutionException e) {
        } catch (NullPointerException e) {
        }
        return qaReference;
    }

    /**
     * Waits until a certain goal is fulfilled
     *
     * @param qa      the qactor that will solve the goal
     * @param goal    the goal to be solved
     * @param varName the goal value to be checked
     * @param until   the value the varName should assume to proceed
     */
    public void waitGoalUntil(QActor qa, String goal, String varName, String until) {
        try {
            while (!qa.solveGoal(goal).getVarValue(varName).toString().equals(until)) {
                System.out.print(".");
                Utils.sleep(100);
            }
            System.out.println();
        } catch (NoSolutionException e) {
            e.printStackTrace();
        }
    }

}
