package unit;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import ddr.project.shared.Constants;
import ddr.project.shared.QActorMessageSender;
import it.unibo.ddrbombdetection.Ddrbombdetection;
import it.unibo.tester.Tester;

public class TestBombDetectionTool extends TestingUtils {

    private Ddrbombdetection bombDetection;
    private Tester tester;
    private final QActorMessageSender qaMessageSender = new QActorMessageSender();
    private final String bombMessageId = "mockPhotoMsg";
    private final String detectionToolMessageId = "detectionTool";

    private final String bombMessageContent(String image) {
        return String.format("mockPhotoMsg(\"%s\")", Constants.appendToImagesDir(image));
    }

    private final String detectionToolMessageContent(String isBomb) {
        return String.format("result(\"%s\")", isBomb);
    }

    @Before
    public void setUp() {
        System.out.println("================== setUp ==================");
        new Thread(() -> {
            try {
                it.unibo.ctxRobotRetriever.MainCtxRobotRetriever.initTheContext();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        try {
            it.unibo.ctxRobotExplorer.MainCtxRobotExplorer.initTheContext();
        } catch (Exception e) {
            e.printStackTrace();
        }

        bombDetection = getInitializedQaReference("ddrbombdetection");
        tester = getInitializedQaReference("tester");
    }

    @Test
    public void testBombDetection() {
        Prolog testerEngine = tester.getPrologEngine();
        SolveInfo solveInfo;
        try {
            solveInfo = testerEngine.solve("bombDetected(X).");
            checkResult(solveInfo, "false", "X");

            // send bag inspection message
            qaMessageSender.sendMessage(tester, bombDetection.getNameNoCtrl(), Constants.DISPATCH_MSG, bombMessageId,
                    bombMessageContent("bag"));

            // send no bomb detected message
            qaMessageSender.sendMessage(tester, bombDetection.getNameNoCtrl(), Constants.DISPATCH_MSG,
                    detectionToolMessageId, detectionToolMessageContent("no"));

            waitGoalUntil(tester, "testNoBomb(X)", "X", "true");

            solveInfo = testerEngine.solve("bombDetected(X).");
            checkResult(solveInfo, "false", "X");

            // send another bag inspection message
            qaMessageSender.sendMessage(tester, bombDetection.getNameNoCtrl(), Constants.DISPATCH_MSG, bombMessageId,
                    bombMessageContent("bomb.png"));

            // send bomb detected message
            qaMessageSender.sendMessage(tester, bombDetection.getNameNoCtrl(), Constants.DISPATCH_MSG,
                    detectionToolMessageId, detectionToolMessageContent("yes"));

            waitGoalUntil(tester, "testIsBomb(X)", "X", "true");

            solveInfo = testerEngine.solve("bombDetected(X).");
            checkResult(solveInfo, "true", "X");
        } catch (MalformedGoalException e) {
            e.printStackTrace();
        } catch (NoSolutionException e) {
            e.printStackTrace();
        }
    }

}
