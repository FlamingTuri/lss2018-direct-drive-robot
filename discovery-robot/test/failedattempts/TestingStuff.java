package failedattempts;

import org.junit.Before;
import org.junit.Test;

import ddr.project.shared.QActorMessageSender;
import ddr.project.shared.QActorsConstants;
import ddr.project.shared.Utils;
import it.unibo.ddrenvobserverexplorer.Ddrenvobserverexplorer;
import it.unibo.ddrmindexplorer.Ddrmindexplorer;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import unit.TestingUtils;

public class TestingStuff extends TestingUtils {

    private QActorContext context;
    private Ddrenvobserverexplorer envObserver;
    private Ddrmindexplorer mind;
    private final QActorMessageSender qaMessageSender = new QActorMessageSender();
    private final String temperatureMessageId = "temperature";
    private static boolean operationsEnded = false;

    private final String temperatureMessageContent(int temperature) {
        return String.format("temperature(%s)", temperature);
    }

    @Before
    public void setUp() {
        System.out.println("================== setUp ==================");
        try {
            context = it.unibo.ctxRobotExplorer.MainCtxRobotExplorer.initTheContext();
            Utils.sleep(5000); // give the time to start and execute
            envObserver = (Ddrenvobserverexplorer) getQActorForTesting(QActorsConstants.explorerEnvObserver);
            mind = (Ddrmindexplorer) getQActorForTesting(QActorsConstants.explorerMind);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Test
    public void testEnvCondsChange() {
        waitQactorsInitialization(envObserver, mind);

//        Prolog mindEngine = mind.getPrologEngine();
//        SolveInfo solveInfo;
//        try {
//            solveInfo = mindEngine.solve("envCond(X).");
//            checkResult(solveInfo, "f", "X");
//
//            int newTemperature = 23;
//            qaMessageSender.selfMessage(envObserver, Constants.DISPATCH_MSG, temperatureMessageId,
//                    temperatureMessageContent(newTemperature));
//
//            sleep(3000); // waiting values update
//
//            solveInfo = mindEngine.solve("envCond(X).");
//            checkResult(solveInfo, "t", "X");
//        } catch (MalformedGoalException e) {
//            e.printStackTrace();
//        } catch (NoSolutionException e) {
//            e.printStackTrace();
//        }
//
//        mind.createSimulatedActor();

        // new CustomQActor("tester", context, null);

//        QActorMessage message = null;
//        try {
//            message = new QActorMessage("temperature", Constants.DISPATCH_MSG, "asd", "aa", "temperature(23)", "1");
//        } catch (Exception e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//        try {
//            mind.onReceive(message);
//        } catch (Throwable e1) {
//            e1.printStackTrace();
//        }
//        mind.createSimulatedActor();
//
//        try {
//            mind.planUtils.execThePlan(10000, "temperature", "temperature(23)", "");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

}