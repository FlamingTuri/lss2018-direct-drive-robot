package failedattempts;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.PlanRepeat;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.QActorUtils;
import it.unibo.qactors.StateFun;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;

public class CustomQActor extends QActor {

    public CustomQActor(String actorId, QActorContext myCtx, IOutputEnvView outEnvView) {
        super(actorId, myCtx, outEnvView);
    }

    @Override
    protected void doJob() throws Exception {
        String name = getName().replace("_ctrl", "");
        mysupport = (IMsgQueue) QActorUtils.getQActor(name);
        initStateTable();
        initSensorSystem();
        history.push(stateTab.get("init"));
        autoSendStateExecMsg();
    }

    private void initStateTable() {
        // stateTab.put("handleToutBuiltIn", handleToutBuiltIn);
        // stateTab.put("init", init);
    }

    StateFun handleToutBuiltIn = () -> {
        try {
            PlanRepeat pr = PlanRepeat.setUp("handleTout", -1);
            String myselfName = "handleToutBuiltIn";
            println("tester tout : stops");
            repeatPlanNoTransition(pr, myselfName, "application_" + myselfName, false, false);
        } catch (Exception e_handleToutBuiltIn) {
            println(getName() + " plan=handleToutBuiltIn WARNING:" + e_handleToutBuiltIn.getMessage());
            QActorContext.terminateQActorSystem(this);
        }
    };

    StateFun init = () -> {
        try {
            PlanRepeat pr = PlanRepeat.setUp("init", -1);
            String myselfName = "init";
            // switchTo setup
            // switchToPlanAsNextState(pr, myselfName, "tester_" + myselfName, "setup", false, false, null);
        } catch (Exception e_init) {
            println(getName() + " plan=init WARNING:" + e_init.getMessage());
            QActorContext.terminateQActorSystem(this);
        }
    };

    private void initSensorSystem() {
    }

    public void addState(String stateName, StateFun behaviour) {
        stateTab.put(stateName, behaviour);
    }
}
