package ddr.project.robot;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import ddr.project.consoleupdater.ConsoleUpdater;
import ddr.project.consoleupdater.SocketioConsoleUpdater;
import ddr.project.shared.Constants;
import ddr.project.shared.QActorsConstants;
import ddr.project.shared.Utils;

/**
 * Class used to simulate robot explorer operations after finishing the exploration
 */
public class VirtualRobotExplorer {

    private static final String retrieverAddress = "localhost";
    // private static final String retrieverAddress = "192.168.43.136";
    private static final int retrieverPort = 8027;
    private Socket clientSocket;
    private DataOutputStream outToServer;
    private final String sender = "mockExplorer";
    private int seqNum = 1;

    public VirtualRobotExplorer(String host, int port) {
        try {
            clientSocket = new Socket(host, port);
            outToServer = new DataOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a dispatch message to a qactor
     * 
     * @param msgId
     * @param payload
     * @param receiver the name of the receiving qactor
     */
    public void sendMessage(String msgId, String payload, String receiver) {
        String message = String.format("msg(%s,%s,%s,%s,%s,%d)", msgId, Constants.DISPATCH_MSG, sender, receiver,
                payload, seqNum++);
        try {
            outToServer.writeBytes(message + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String prologMap = "r00xn100xn100xn1xxxn111xn";
        int bombX = 4;
        int bombY = 3;
        String payload = String.format("mapRetriever(%s,%d,%d)", prologMap, bombX, bombY);
        VirtualRobotExplorer virtualExplorer = null;
        try {
            virtualExplorer = new VirtualRobotExplorer(retrieverAddress, retrieverPort);
            virtualExplorer.sendMessage("mapRetriever", payload, QActorsConstants.retrieverMind);
        } catch (Exception e) {
        }

        ConsoleUpdater consoleUpdater = new SocketioConsoleUpdater(null);
        consoleUpdater.extendMap();
        consoleUpdater.extendMap();
        consoleUpdater.extendMap();

        int x = 0;
        int y = 0;
        for (int i = 0; i < prologMap.length(); i++) {
            char c = prologMap.charAt(i);
            if (c == 'n') {
                y = 0;
                x++;
            } else {
                consoleUpdater.updateCoordinates("" + c, x, y);
                y++;
            }
        }
        consoleUpdater.updateCoordinates("B", bombX, bombY);

        consoleUpdater.switchCmdMode(true);
        Utils.sleep(3000);
        System.exit(0);
    }

}
