package ddr.project.robot;

import ddr.project.led.Led;
import it.unibo.qactors.akka.QActor;

public abstract class AbstractRobot implements Robot {

    protected Led led;
    private boolean stop;

    @Override
    public abstract void setup(QActor qa, String port);

    @Override
    public abstract void doMove(QActor qa, String cmd);

    @Override
    public abstract void doMove(QActor qa, String cmd, String duration);

    @Override
    public void startBlinking(QActor qa) {
        stop = false;
        Thread blinker = new Thread() {
            @Override
            public void run() {
                while (!stop) {
                    led.switchOn();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    led.switchOff();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        if (led == null) {
            qa.println("led not initialized");
        } else {
            blinker.start();
        }
    }

    @Override
    public void stopBlinking(QActor qa) {
        stop = true;
    }
}
