package ddr.project.robot;

import it.unibo.qactors.akka.QActor;

public interface Robot {

    /**
     * Initializes the robot
     * 
     * @param qa
     * @param port
     */
    void setup(QActor qa, String port);

    /**
     * Makes the robot move. The available moves are:
     *  - w: moves forward
     *  - a: turn left
     *  - s: move backwards
     *  - d: turn right
     *  - h: stop
     * 
     * @param qa
     * @param cmd the command to run
     */
    void doMove(QActor qa, String cmd);

    /**
     * Makes robot move in a specified direction for the specified duration
     * 
     * @param qa
     * @param cmd      the command to run
     * @param duration the time in milliseconds the robot should do the move
     */
    void doMove(QActor qa, String cmd, String duration);

    /**
     * Makes the led on the robot start blinking
     * 
     * @param qa
     */
    void startBlinking(QActor qa);

    /**
     * Stops the led blinking
     * 
     * @param qa
     */
    void stopBlinking(QActor qa);

    /**
     * Enables the proximity sonar on the robot
     * 
     * @param qa
     */
    void enableSonar(QActor qa);
}
