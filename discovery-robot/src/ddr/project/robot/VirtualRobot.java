package ddr.project.robot;

import ddr.it.unibo.utils.clientTcpForVirtualRobot;
import ddr.project.led.VirtualLed;
import it.unibo.qactors.akka.QActor;

public class VirtualRobot extends AbstractRobot {

    public VirtualRobot() {
        this(null, "mock");
    }

    public VirtualRobot(QActor qa, String hostNameStr) {
        setup(qa, hostNameStr);
        led = new VirtualLed();
    }

    @Override
    public void setup(QActor qa, String hostNameStr) {
        try {
            qa.println("robotVirtual setUp " + hostNameStr);
            clientTcpForVirtualRobot.initClientConn(qa, hostNameStr, "8999");
        } catch (Exception e) {
            e.printStackTrace();
        }
        led = new VirtualLed();
    }

    @Override
    public void doMove(QActor qa, String cmd) {
        String jsonString;
        switch (cmd) {
        case "h":
            jsonString = "{'type': 'alarm', 'arg': 0 }";
            break;
        case "w":
            jsonString = "{'type': 'moveForward', 'arg': -1 }";
            break;
        case "a":
            jsonString = "{'type': 'turnLeft', 'arg': 400 }";
            break;
        case "d":
            jsonString = "{'type': 'turnRight', 'arg': 400 }";
            break;
        case "s":
            jsonString = "{'type': 'moveBackward', 'arg': -1 }";
            break;
        default:
            jsonString = null;
        }
        if (jsonString != null) {
            clientTcpForVirtualRobot.sendMsg(qa, jsonString);
        }
    }

    @Override
    public void doMove(QActor qa, String cmd, String duration) {
        String jsonString;
        switch (cmd) {
        case "h":
            jsonString = "{'type': 'alarm', 'arg': " + duration + " }";
            break;
        case "w":
            jsonString = "{'type': 'moveForward', 'arg': " + duration + " }";
            break;
        case "a":
            jsonString = "{'type': 'turnLeft', 'arg': " + duration + " }";
            break;
        case "d":
            jsonString = "{'type': 'turnRight', 'arg': " + duration + " }";
            break;
        case "s":
            jsonString = "{'type': 'moveBackward', 'arg': " + duration + " }";
            break;
        default:
            jsonString = null;
        }
        if (jsonString != null) {
            clientTcpForVirtualRobot.sendMsg(qa, jsonString);
            try {
                Thread.sleep(Integer.parseInt(duration));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void enableSonar(QActor qa) {
        // do nothing because the virtual sonar is always enabled
    }

    public static void main(String[] args) {
        Robot robot = new VirtualRobot();
        robot.startBlinking(null);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        robot.stopBlinking(null);
    }
}
