package ddr.project.robot;

import ddr.it.unibo.robotOnRaspOnly.SonarInCObserver;
import ddr.project.led.RaspberryLed;
import ddr.project.shared.BashScriptRunner;
import ddr.project.shared.Utils;
import it.unibo.qactors.akka.QActor;

public class RaspberryRobot extends AbstractRobot {

    private static final String scriptFolder = "doMoveScripts/";
    private BashScriptRunner bashScriptRunner;
    private SonarInCObserver sonarInCObserver;

    public RaspberryRobot() {
    }

    public RaspberryRobot(QActor qa) {
        setup(qa);
    }

    /**
     * Initializes the robot components necessary to move, blink and sense distance from sonar
     * 
     * @param qa
     */
    public void setup(QActor qa) {
        if (bashScriptRunner == null) {
            bashScriptRunner = new BashScriptRunner();
            bashScriptRunner.run(scriptFolder + "init");
            sonarInCObserver = new SonarInCObserver(qa);
            led = new RaspberryLed();
        }
    }

    @Override
    public void setup(QActor qa, String port) {
        setup(qa);
    }

    @Override
    public void doMove(QActor qa, String cmd) {
        cmd = Utils.removeLeadingAndTrailingApostrophe(cmd);
        bashScriptRunner.run(scriptFolder + cmd);
    }

    @Override
    public void doMove(QActor qa, String cmd, String duration) {
        cmd = Utils.removeLeadingAndTrailingApostrophe(cmd);
        doMove(qa, cmd);
//        duration = Utils.removeLeadingAndTrailingApostrophe(duration);
//        long longDuration = Long.valueOf(duration);
//        if (!(cmd.equals("h") || cmd.equals("w"))) {
//            new Thread(() -> {
//                Utils.sleep(longDuration);
//                doMove(qa, "h");
//            }).start();
//        }
    }

    @Override
    public void enableSonar(QActor qa) {
        sonarInCObserver.enableSonar(qa);
    }

}
