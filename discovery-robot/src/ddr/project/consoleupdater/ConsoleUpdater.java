package ddr.project.consoleupdater;

public interface ConsoleUpdater {

    /**
     * Sends to the console-node the new robot status
     *
     * @param robotType the robot name
     * @param status    the robot status
     */
    void sendNewState(String robotType, String status);

    /**
     * Tells to the console-node to increase the map size by 1
     */
    void extendMap();

    /**
     * Sends to the console-node the new status of a cell
     *
     * @param symbol the status of the cell
     * @param x      the x coordinate
     * @param y      the y coordinate
     */
    void updateCoordinates(String symbol, int x, int y);

    /**
     * Tells to the console-node to switch to the other robot command mode
     */
    void switchCmdMode(boolean bombFound);

    /**
     * Enables/disables a button
     *
     * @param button the button to enable/disable
     * @param status true to
     */
    void disableButton(String button, boolean enable);

    /**
     * Tells the console if the environment conditions are dangerous for the robot work
     *
     * @param danger true if the current environment conditions are dangerous for the robot work
     */
    void environmentConditionsChange(boolean danger);
}
