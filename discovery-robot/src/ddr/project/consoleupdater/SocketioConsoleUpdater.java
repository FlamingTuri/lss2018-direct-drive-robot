package ddr.project.consoleupdater;

import org.json.JSONObject;

import akka.japi.Pair;
import ddr.it.unibo.planning.RoomMap;
import ddr.project.shared.AbstractSocketioClient;
import ddr.project.shared.Constants;
import ddr.project.shared.Utils;
import ddr.project.shared.Constants.RobotType;
import it.unibo.qactors.akka.QActor;

public class SocketioConsoleUpdater extends AbstractSocketioClient implements ConsoleUpdater {

    private static final String newStatusEvent = "robot-state";
    private static final String extendMapEvent = "extend-map";
    private static final String coordinatesUpdateEvent = "coordinates-update";
    private static final String switchCmdModeEvent = "switch-cmd-mode";
    private static final String changeButtonStatusEvent = "change-button-status";
    private static final String environmentConditionsEvent = "environment-conditions-change";
    private static final String userCmdEvent = "usercmd";

    public SocketioConsoleUpdater(QActor qa) {
        super(Constants.CONSOLE_ADDRESS, Constants.CONSOLE_PORT);
        initQaMessageSender(qa);
    }

    @Override
    protected void setupEventListeners() {
        String name = this.getClass().getSimpleName();
        setupDefaultEventListeners(name, "to console", "from console");
        socket.on(userCmdEvent, args -> {
            emitEventForEachPayloadMsg(userCmdEvent, args);
        });
    }

    @Override
    public void sendNewState(String robotType, String status) {
        JSONObject jsonStatus = new JSONObject();
        robotType = Utils.removeLeadingAndTrailingApostrophe(robotType);
        jsonStatus.put("robotType", robotType);
        status = Utils.removeLeadingAndTrailingApostrophe(status);
        jsonStatus.put("status", status);
        socket.emit(newStatusEvent, jsonStatus);
    }

    @Override
    public void extendMap() {
        RoomMap roomMap = RoomMap.getRoomMap();
        socket.emit(extendMapEvent, new Pair<>(roomMap.getDimX(), roomMap.getDimY()));
    }

    @Override
    public void updateCoordinates(String symbol, int x, int y) {
        JSONObject coordinates = new JSONObject();
        symbol = Utils.removeLeadingAndTrailingApostrophe(symbol);
        coordinates.put("symbol", symbol);
        coordinates.put("x", x);
        coordinates.put("y", y);
        socket.emit(coordinatesUpdateEvent, coordinates);
    }

    @Override
    public void switchCmdMode(boolean bombFound) {
        socket.emit(switchCmdModeEvent, bombFound);
    }

    @Override
    public void disableButton(String button, boolean status) {
        JSONObject buttonStatus = new JSONObject();
        button = Utils.removeLeadingAndTrailingApostrophe(button);
        buttonStatus.put("button", button);
        buttonStatus.put("disable", status);
        socket.emit(changeButtonStatusEvent, buttonStatus);
    }

    @Override
    public void environmentConditionsChange(boolean danger) {
        // Utils.log("" + danger);
        socket.emit(environmentConditionsEvent, danger);
    }

    public static void main(String[] args) {
        ConsoleUpdater consoleUpdaterExplorer = new SocketioConsoleUpdater(null);
        ConsoleUpdater consoleUpdaterRetriever = new SocketioConsoleUpdater(null);

        Utils.sleep(500);
        // status update
        consoleUpdaterExplorer.sendNewState(Constants.RobotType.EXPLORER.getType(), "exploring");
        consoleUpdaterRetriever.sendNewState(Constants.RobotType.RETRIEVER.getType(), "retrieving");

        // map update
        consoleUpdaterExplorer.extendMap();
        consoleUpdaterExplorer.extendMap();
        int k = 0;
        int size = 3;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // Utils.log("" + k);
                consoleUpdaterExplorer.updateCoordinates(k + "", i, j);
                k++;
            }
        }

        // out of boundary coordinates update
        consoleUpdaterExplorer.updateCoordinates("X", 1, 3);
        consoleUpdaterExplorer.updateCoordinates("B", 4, 2);

        // switch command mode
        consoleUpdaterExplorer.switchCmdMode(false);

        // disable retrieve button
        // consoleUpdater.disableButton("retrieveButton", true);

        // show danger message
        consoleUpdaterExplorer.environmentConditionsChange(true);

        Utils.sleep(4000);
        // hide danger message
        consoleUpdaterExplorer.environmentConditionsChange(false);

        consoleUpdaterRetriever.sendNewState(Constants.RobotType.EXPLORER.getType(), "ended exploration");
        consoleUpdaterRetriever.sendNewState(Constants.RobotType.RETRIEVER.getType(), "ended retrieval");

        Utils.log("everything sent");
        System.exit(0);
    }

}
