package ddr.project.shared;

public class Utils {

    /**
     * Java Thread.sleep wrapper. Shortcut to avoid try - catch
     * 
     * @param millis the milliseconds to sleep
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * When a qa sends a string it is wrapped between '' causing problems
     *
     * @param s the string sent by a qa
     * @return the string without leading and trailing apostrophe
     */
    public static String removeLeadingAndTrailingApostrophe(String s) {
        return s.replaceAll("^'+", "").replaceAll("'+$", "");
    }

    /**
     * Logging utility. Print format [{caller class name}]: message (line {caller line number})
     * 
     * @param message the message to log
     */
    public static void log(String message) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String callerClassName = stackTrace[2].getClassName();
        int callerLineNumber = stackTrace[2].getLineNumber();
        String toPrint = String.format("[%s]: %s (line %d)", callerClassName, message, callerLineNumber);
        System.out.println(toPrint);
    }

}
