package ddr.project.shared;

import java.util.Objects;

/**
 * Utility class to check the correct initialization of setup of our application objects
 *
 */
public class SetupChecker {

    /**
     * Checks that each input object is not null
     * 
     * @param args the objects to verify
     */
    protected static void checkSetupNotNull(Object... args) {
        for (Object o : args) {
            Objects.requireNonNull(o);
        }
    }
}
