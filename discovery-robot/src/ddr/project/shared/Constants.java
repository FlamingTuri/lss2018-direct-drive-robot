package ddr.project.shared;

public class Constants {

    public static final String DISPATCH_MSG = "dispatch";
    public static final String TEMPERATURE_MSG_ID = "temperature";
    public static final String CONSOLE_ADDRESS = "localhost";
    // public static final String CONSOLE_ADDRESS = "192.168.43.65";
    public static final int CONSOLE_PORT = 3000;
    public static final String TEMPERATURE_SERVICE_ADDRESS = "localhost";
    // public static final String TEMPERATURE_SERVICE_PORT = "192.168.43.65";
    public static final int TEMPERATURE_SERVICE_PORT = 3333;
    public static final String IMAGES_DIR = "images/";
    
    public static String appendToImagesDir(String imageName) {
        return IMAGES_DIR + imageName;
    }

    public enum RobotType {
        EXPLORER("explorer"), RETRIEVER("retriever");

        private final String robotType;

        private RobotType(String robotType) {
            this.robotType = robotType;
        }

        public String getType() {
            return this.robotType;
        }
    }
}
