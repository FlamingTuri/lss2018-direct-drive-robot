package ddr.project.shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BashScriptRunner {

    private final Runtime runtime;

    public BashScriptRunner() {
        runtime = Runtime.getRuntime();
    }

    /**
     * Runs a bash script
     * 
     * @param scriptName the script name with it's relative path
     * @return the running process
     */
    public Process run(String scriptName) {
        String script = "./" + scriptName + ".sh";
        String[] cmdarray = { script };
        Process p = null;
        try {
            p = runtime.exec(cmdarray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }

    public static void main(String[] args) {
        BashScriptRunner bashScriptRunnner = new BashScriptRunner();
        Process p = bashScriptRunnner.run("testingScripts/testMotors");
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String data;
        try {
            data = reader.readLine();
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}