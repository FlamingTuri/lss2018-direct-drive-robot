package ddr.project.shared;

public class QActorsConstants {
    // explorer robot
    public static final String explorerMind = "ddrmindexplorer";
    public static final String explorerPlayer = "ddrplayerexplorer";
    public static final String explorerEnvObserver = "ddrenvobserverexplorer";

    // retriever robot
    public static final String retrieverMind = "ddrmindretriever";
    public static final String retrieverPlayer = "ddrplayerretriever";
    
    public static final String tester = "tester";
}