package ddr.project.shared;

import java.util.Optional;

import ddr.project.temperature.ContentFormatter;
import it.unibo.qactors.akka.QActor;

public class QActorMessageSender {

    protected final QActor qa;
    private final ContentFormatter formatter;

    public QActorMessageSender() {
        this(null, null);
    }

    public QActorMessageSender(QActor qa) {
        this(qa, null);
    }

    public QActorMessageSender(ContentFormatter formatter) {
        this(null, formatter);
    }

    public QActorMessageSender(QActor qa, ContentFormatter formatter) {
        this.qa = qa;
        this.formatter = Optional.ofNullable(formatter).orElse(message -> message);
    }

    public String wrapArgument(String arg) {
        return "[" + arg + "]";
    }

    public void sendMessage(String senderName, String destinationName, String messageType, String messageId,
            String messageContent) {
        sendMessage(qa, destinationName, messageType, messageId, messageContent);
    }

    public void sendMessage(QActor qa, String destinationName, String messageType, String messageId,
            String messageContent) {
        String senderName = Optional.of(qa.getNameNoCtrl()).orElse("undefined");
        String qaFormattedMessageContent = formatter.format(messageContent);
        String debug = wrapArgument(senderName) + " sent " + wrapArgument(messageType) + " message "
                + wrapArgument(messageId + " : " + qaFormattedMessageContent) + " to " + wrapArgument(destinationName);
        System.out.println(debug);
        if (qa != null) {
            try {
                qa.sendMsg(senderName, messageId, destinationName, messageType, qaFormattedMessageContent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void selfMessage(String messageType, String messageId, String messageContent) {
        selfMessage(qa, messageType, messageId, messageContent);
    }

    public void selfMessage(QActor qa, String messageType, String messageId, String messageContent) {
        sendMessage(qa, qa.getNameNoCtrl(), messageType, messageId, messageContent);
    }

    public void emitEvent(String eventID, String eventContent) {
        emitEvent(qa, eventID, eventContent);
    }

    public void emitEvent(QActor qa, String eventID, String eventContent) {
        String qaFormattedEventContent = formatter.format(eventContent);
        System.out.println("emitted event " + wrapArgument(eventID + " : " + qaFormattedEventContent));
        if (qa != null) {
            try {
                qa.emit(eventID, qaFormattedEventContent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
