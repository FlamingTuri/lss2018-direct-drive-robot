package ddr.project.shared;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class Base64Converter {

    private final Encoder encoder = Base64.getEncoder();
    private final Decoder decoder = Base64.getDecoder();

    public Encoder getEncoder() {
        return encoder;
    }

    public Decoder getDecoder() {
        return decoder;
    }

    public String encodeToBase64(byte[] bytesToEncode) {
        return encoder.encodeToString(bytesToEncode);
    }

    public byte[] deencodeFromBase64(byte[] bytesToDecode) {
        return decoder.decode(bytesToDecode);
    }

    public String encodeImage(String path) {
        String base64Image = "";
        File file = new File(path);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = encodeToBase64(imageData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return base64Image;
    }

    /**
     * File does not work in jar
     * 
     * @param path image path
     * @return
     */
    public String encodeImageFromStreamReader(String path) {
        String base64Image = "";
        InputStream is = this.getClass().getResourceAsStream(path);
        InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        try (BufferedReader bf = new BufferedReader(isr)) {
            // Reading a Image file from file system
            byte imageData[] = bf.lines().reduce((a, b) -> a.concat(b)).orElse("").getBytes();
            base64Image = encodeToBase64(imageData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64Image;
    }

    public static void main(String[] args) {
        Base64Converter converter = new Base64Converter();
        String encodedData;
        String decodedData;

        encodedData = converter.encodeToBase64("test".getBytes());
        Utils.log(encodedData);
        decodedData = new String(converter.deencodeFromBase64(encodedData.getBytes()));
        Utils.log(decodedData);

        encodedData = converter.encodeImage(Constants.appendToImagesDir("bag.png"));
        // encodedData = converter.encodeImageFromStreamReader("/bag.png");

        decodedData = converter.encodeImage(Constants.appendToImagesDir("bomb.png"));
        // encodedData = converter.encodeImageFromStreamReader("/bomb.png");

        System.exit(0);
    }
}
