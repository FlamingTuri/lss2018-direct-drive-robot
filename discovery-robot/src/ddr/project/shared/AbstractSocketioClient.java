package ddr.project.shared;

import java.net.URISyntaxException;
import java.util.Objects;

import ddr.project.temperature.ContentFormatter;
import io.socket.client.IO;
import io.socket.client.Socket;
import it.unibo.qactors.akka.QActor;

public abstract class AbstractSocketioClient {

    private final String address;
    private final int port;
    protected Socket socket;
    protected QActorMessageSender qaMessageSender;

    public AbstractSocketioClient(String address, int port) {
        this.address = address;
        this.port = port;
        init();
    }

    /**
     * Connects the Socket.io client at the specified address and port
     */
    protected void init() {
        try {
            socket = IO.socket("http://" + address + ":" + port);

            setupEventListeners();

            // start connection
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setups the Socket.io client event listeners.
     */
    protected abstract void setupEventListeners();

    /**
     * Setups listeners for connection/disconnection events
     * 
     * @param name string printed when the above events happens
     */
    protected void setupDefaultEventListeners(String name) {
        setupDefaultEventListeners(name, "", "");
    }

    /**
     * Setups listeners for connection/disconnection events
     * 
     * @param name              string printed when the above events happens
     * @param connectMessage    additional connection message
     * @param disconnectMessage additional disconnection message
     */
    protected void setupDefaultEventListeners(String name, String connectMessage, String disconnectMessage) {
        socket.on(io.socket.client.Socket.EVENT_CONNECT, args -> {
            System.out.println(name + " connected " + connectMessage);
        }).on(io.socket.client.Socket.EVENT_DISCONNECT, args -> {
            System.out.println(name + " disconnected " + disconnectMessage);
        });
    }

    /**
     * Emits an event
     * 
     * @param event event name
     * @param args  event args
     */
    public void emit(String event, Object... args) {
        socket.emit(event, args);
    }

    /**
     * Initializes the utility to send messages and events to the qactor
     * 
     * @param qa
     */
    protected void initQaMessageSender(QActor qa) {
        qaMessageSender = new QActorMessageSender(qa);
    }

    /**
     * Initializes the utility to send messages and events to the qactor
     * 
     * @param qa
     * @param formatter the message formatter
     */
    protected void initQaMessageSender(QActor qa, ContentFormatter formatter) {
        qaMessageSender = new QActorMessageSender(qa, formatter);
    }

    /**
     * Send a message to the qactor specified at QActorMessageSender initialization
     * 
     * @param messageType
     * @param messageId
     * @param args        arguments in the payload
     */
    protected void selfMsgForEachPayloadMsg(String messageType, String messageId, Object[] args) {
        Objects.requireNonNull(qaMessageSender);
        for (Object s : args) {
            String messageContent = s.toString();
            qaMessageSender.selfMessage(messageType, messageId, messageContent);
        }
    }

    /**
     * Send an event using the qactor specified at QActorMessageSender initialization
     * 
     * @param eventId
     * @param args    arguments in the payload
     */
    protected void emitEventForEachPayloadMsg(String eventId, Object[] args) {
        Objects.requireNonNull(qaMessageSender);
        for (Object s : args) {
            String messageContent = s.toString();
            qaMessageSender.emitEvent(eventId, messageContent);
        }
    }

}
