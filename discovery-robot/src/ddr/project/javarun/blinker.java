package ddr.project.javarun;

import ddr.project.led.Led;
import ddr.project.shared.SetupChecker;
import it.unibo.qactors.akka.QActor;

public class blinker extends SetupChecker {

    private static boolean stop;
    private static Led led;

    public static void setup(QActor qa, Led ledImpl) {
        led = ledImpl;
    }

    public static void startBlinking(QActor qa, Led ledImpl) {
        setup(qa, ledImpl);
        startBlinking(qa);
    }

    public static void startBlinking(QActor qa) {
        checkSetupNotNull(led);
        stop = false;
        led.switchOff();
        Thread blinker = new Thread() {
            @Override
            public void run() {
                while (!stop) {
                    led.switchOn();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    led.switchOff();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                led.switchOff();
            }
        };
        if (led == null) {
            System.err.println("No led initialized");
        } else {
            blinker.start();
        }
    }

}
