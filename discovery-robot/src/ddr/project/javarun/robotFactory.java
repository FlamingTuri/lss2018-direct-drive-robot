package ddr.project.javarun;

import java.util.HashMap;
import java.util.Map;

import ddr.project.robot.RaspberryRobot;
import ddr.project.robot.Robot;
import ddr.project.robot.VirtualRobot;
import ddr.project.shared.SetupChecker;
import it.unibo.qactors.akka.QActor;

public class robotFactory extends SetupChecker {

    private static Map<QActor, Robot> robots = new HashMap<>();

    public static void setupRaspberryRobot(QActor qa) {
        robots.put(qa, new RaspberryRobot(qa));
    }

    public static void setupVirtualRobot(QActor qa, String hostname) {
        robots.put(qa, new VirtualRobot(qa, hostname));
    }

    private static Robot getRobot(QActor qa) {
        Robot robot = robots.get(qa);
        checkSetupNotNull(robot);
        return robot;
    }

    public static void doMove(QActor qa, String cmd) {
        Robot robot = getRobot(qa);
        robot.doMove(qa, cmd);
    }

    public static void doMove(QActor qa, String cmd, String duration) {
        Robot robot = getRobot(qa);
        robot.doMove(qa, cmd, duration);
    }

    public static void startBlinking(QActor qa) {
        Robot robot = getRobot(qa);
        robot.startBlinking(qa);
    }

    public static void stopBlinking(QActor qa) {
        Robot robot = getRobot(qa);
        robot.stopBlinking(qa);
    }
    
    public static void enableSonar(QActor qa) {
    	getRobot(qa).enableSonar(qa);
    }
}
