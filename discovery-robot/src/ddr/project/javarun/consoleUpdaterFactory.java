package ddr.project.javarun;

import ddr.project.consoleupdater.ConsoleUpdater;
import ddr.project.consoleupdater.SocketioConsoleUpdater;
import ddr.project.shared.SetupChecker;
import ddr.project.shared.Utils;
import it.unibo.qactors.akka.QActor;

public class consoleUpdaterFactory extends SetupChecker {

    private static ConsoleUpdater console;

    public static void setupSocketioConsole(QActor qa) {
        console = new SocketioConsoleUpdater(qa);
    }

    public static void sendNewState(QActor qa, String robotName, String status) {
        checkSetupNotNull(console);
        Utils.log(robotName);
        console.sendNewState(robotName, status);
    }

    public static void extendMap(QActor qa) {
        checkSetupNotNull(console);
        console.extendMap();
    }

    public static void changeRobotCmd(QActor qa, String bombFound) {
        checkSetupNotNull(console);
        bombFound = Utils.removeLeadingAndTrailingApostrophe(bombFound);
        boolean hasBombBeenFound = bombFound.equals("t") ? true : false;
        // Utils.log("" + hasBombBeenFound);
        console.switchCmdMode(hasBombBeenFound);
    }

    public static void updateCoordinates(QActor qa, String symbol, String x, String y) {
        checkSetupNotNull(console);
        console.updateCoordinates(symbol, Integer.parseInt(x), Integer.parseInt(y));
    }

    public static void disableButton(QActor qa, String button, String disable) {
        checkSetupNotNull(console);
        button = Utils.removeLeadingAndTrailingApostrophe(button);
        disable = Utils.removeLeadingAndTrailingApostrophe(disable);
        console.disableButton(button, getBoolean(disable));
    }

    public static void notifyEnvironmentConditions(QActor qa, String danger) {
        checkSetupNotNull(console);
        danger = Utils.removeLeadingAndTrailingApostrophe(danger);
        console.environmentConditionsChange(getBoolean(danger));
    }

    private static boolean getBoolean(String boolString) {
        // java here is an interesting language: even when receiving "true"
        // Boolean.getBoolean(); returns false
        // Utils.log(boolString);
        // Utils.log(""+ Boolean.getBoolean(boolString));
        return boolString.equals("true") ? true : false;
    }
}
