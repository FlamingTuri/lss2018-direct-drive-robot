package ddr.project.javarun;

import java.util.HashSet;
import java.util.Set;

import cli.System.NotImplementedException;
import ddr.project.temperature.ContentFormatter;
import ddr.project.temperature.SocketioTemperatureRetriever;
import ddr.project.temperature.TemperatureRetriever;
import it.unibo.qactors.akka.QActor;

public class temperatureRetrieverFactory {

    private static ContentFormatter formatter;
    private static Set<TemperatureRetriever> temperatureTaker = new HashSet<>();

    public static void setupMyFormatter(QActor qa) {
        formatter = message -> String.format("temperature(%s)", message);
    }

    public static void setupSocketioRetriever(QActor qa) {
        temperatureTaker.add(new SocketioTemperatureRetriever(qa, formatter));
    }

    public static void setupMqttRetriever(QActor qa, String args) throws NotImplementedException {
        throw new NotImplementedException();
    }

}
