package ddr.project.javarun;

import ddr.project.bombdetectiontool.BombDetectorTool;
import ddr.project.bombdetectiontool.MockBombDetectorTool;
import ddr.project.bombdetectiontool.SocketioConsoleBombDetectorTool;
import ddr.project.shared.SetupChecker;
import it.unibo.qactors.akka.QActor;

public class bombDetectorToolFactory extends SetupChecker {

    private static BombDetectorTool bombDetectorTool;

    public static void setupMockBombDetectorTool(QActor qa) {
        bombDetectorTool = new MockBombDetectorTool(qa);
    }

    public static void setupConsoleBombDetectorTool(QActor qa) {
        bombDetectorTool = new SocketioConsoleBombDetectorTool(qa);
    }

    public static void runDetectionTool(QActor qa, String imagePath) {
        checkSetupNotNull(bombDetectorTool);
        bombDetectorTool.runDetector(imagePath);
    }
}
