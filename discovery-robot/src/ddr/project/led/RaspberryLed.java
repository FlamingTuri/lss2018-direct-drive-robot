package ddr.project.led;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RaspberryLed extends AbstractLed {

    private final Runtime runtime = Runtime.getRuntime();

    @Override
    protected void apply() {
        String[] cmdarray = { "./switch-pin.sh", "" + on };
        try {
            Process p = runtime.exec(cmdarray);
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String data = reader.readLine();
            System.out.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Led led = new RaspberryLed();
        led.switchOn();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        led.switchOff();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
