package ddr.project.led;

public interface Led {

    /**
     * Switches led on
     */
    void switchOn();

    /**
     * Switches led off
     */
    void switchOff();

    /**
     * Check if the led is on
     *
     * @return true if the led is on
     */
    boolean isOn();

}
