package ddr.project.led;

public abstract class AbstractLed implements Led {

    protected boolean on;

    @Override
    public void switchOn() {
        on = true;
        apply();
    }

    @Override
    public void switchOff() {
        on = false;
        apply();
    }

    /**
     * Performs the action necessary to turn on/off the led
     */
    protected abstract void apply();

    @Override
    public boolean isOn() {
        return on;
    }

}
