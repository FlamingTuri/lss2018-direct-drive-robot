package ddr.project.led;

public class VirtualLed extends AbstractLed {

    @Override
    protected void apply() {
        System.out.println("Led " + (on ? "on" : "off"));
    }
}
