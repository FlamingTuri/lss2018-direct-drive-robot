package ddr.project.bombdetectiontool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ddr.project.shared.Constants;
import ddr.project.shared.QActorMessageSender;
import it.unibo.qactors.akka.QActor;

public class MockBombDetectorTool implements BombDetectorTool {

    private final QActorMessageSender qaMessageSender;
    private final int maxTime;

    public MockBombDetectorTool(QActor qa) {
        this(qa, 5);
    }

    public MockBombDetectorTool(QActor qa, int seconds) {
        qaMessageSender = new QActorMessageSender(qa, message -> String.format("result(%s)", message));
        maxTime = seconds * 1000;
    }

    @Override
    public void runDetector(String imagePath) {
        System.out.print("Is this a bomb? (yes/no): ");
        waitUserInput();
    }

    public void waitUserInput() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        long startTime = System.currentTimeMillis();

        try {
            while ((System.currentTimeMillis() - startTime) < maxTime && !in.ready()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            String messageContent = "no";

            if (in.ready()) {
                System.out.println("You entered: " + in.readLine());
                if (in.readLine().equals("yes")) {
                    messageContent = "yes";
                }
            } else {
                System.out.println("You did not enter data");
                // messageContent = "no";
            }

            qaMessageSender.selfMessage(Constants.DISPATCH_MSG, "detectionTool", messageContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
