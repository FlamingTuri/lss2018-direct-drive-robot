package ddr.project.bombdetectiontool;

import ddr.project.shared.AbstractSocketioClient;
import ddr.project.shared.Base64Converter;
import ddr.project.shared.Constants;
import ddr.project.shared.Utils;
import it.unibo.qactors.akka.QActor;

public class SocketioConsoleBombDetectorTool extends AbstractSocketioClient implements BombDetectorTool {

    private static final String bombDetectionEvent = "bomb-detection";
    private static final String bombDetectionToolResponseEvent = "detectionToolE";
    private final Base64Converter converter;

    public SocketioConsoleBombDetectorTool(QActor qa) {
        super(Constants.CONSOLE_ADDRESS, Constants.CONSOLE_PORT);
        converter = new Base64Converter();
        initQaMessageSender(qa);
    }

    @Override
    protected void setupEventListeners() {
        String name = this.getClass().getSimpleName();
        setupDefaultEventListeners(name, "to console", "from console");
        socket.on(bombDetectionToolResponseEvent, args -> {
            emitEventForEachPayloadMsg(bombDetectionToolResponseEvent, args);
        });
    }

    @Override
    public void runDetector(String imagePath) {
        imagePath = Utils.removeLeadingAndTrailingApostrophe(imagePath);
        String encodedImage = converter.encodeImage(imagePath);
        socket.emit(bombDetectionEvent, encodedImage);
    }

    public static void main(String[] args) {
        // for testing purpose
        BombDetectorTool bombDetectorTool = new SocketioConsoleBombDetectorTool(null);
        Utils.sleep(1000);
        bombDetectorTool.runDetector(Constants.appendToImagesDir("bomb.png"));
        Utils.sleep(1000);
        // socket.disconnect();
        System.exit(0);
    }

}
