package ddr.project.bombdetectiontool;

public interface BombDetectorTool {

    /**
     * Runs the bomb detection tool
     * 
     * @param imagePath the path of the image to analyze
     */
    void runDetector(String imagePath);
}
