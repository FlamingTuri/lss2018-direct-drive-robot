package ddr.project.temperature;

public interface ContentFormatter {

    /**
     * Formats the input message
     *
     * @param message the message to format
     * @return the formatted message
     */
    String format(String message);
}
