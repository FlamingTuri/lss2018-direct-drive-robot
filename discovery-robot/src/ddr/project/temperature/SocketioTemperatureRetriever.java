package ddr.project.temperature;

import ddr.project.shared.AbstractSocketioClient;
import ddr.project.shared.Constants;
import ddr.project.shared.Utils;
import it.unibo.qactors.akka.QActor;

public class SocketioTemperatureRetriever extends AbstractSocketioClient implements TemperatureRetriever {

    private final static String responseTemperatureEvent = "welcomeTemperature";
    private final static String temperatureEvent = "temperature";

    public SocketioTemperatureRetriever(ContentFormatter formatter) {
        this(null, formatter);
    }

    public SocketioTemperatureRetriever(QActor qa, ContentFormatter formatter) {
        super(Constants.TEMPERATURE_SERVICE_ADDRESS, Constants.TEMPERATURE_SERVICE_PORT);
        initQaMessageSender(qa, formatter);
    }

    @Override
    protected void setupEventListeners() {
        String name = this.getClass().getSimpleName();
        setupDefaultEventListeners(name, "to temperature service", "from temperature service");
        socket.on(responseTemperatureEvent, args -> {
            selfMsgForEachPayloadMsg(args);

            // "unsubscribes" from responseTemperature channel
            socket.off(responseTemperatureEvent);
            // "subscribes" to temperature channel
            socket.on(temperatureEvent, args1 -> {
                selfMsgForEachPayloadMsg(args1);
            });
        });
    }

    private void selfMsgForEachPayloadMsg(Object[] args) {
        selfMsgForEachPayloadMsg(Constants.DISPATCH_MSG, Constants.TEMPERATURE_MSG_ID, args);
    }

    public static final void main(String[] args) {
        new SocketioTemperatureRetriever(null, null);
        Utils.sleep(2000);
        // socket.disconnect();
        System.exit(0);
    }

}
