package ddr.it.unibo.planning;

public class Box {
    private boolean isObstacle;
    private boolean isDirty;
    private boolean isRobot;

    public Box(boolean isObstacle, boolean isDirty, boolean isRobot) {
        this.isObstacle = isObstacle;
        this.isDirty = isDirty;
        this.isRobot = isRobot;
    }

    public Box(boolean isObstacle, boolean isDirty) {
        this(isObstacle, isDirty, false);
    }

    public Box() {
        this(false, true);
    }

    public Box copy() {
        return new Box(isObstacle(), isDirty(), isRobot());
    }

    public void setRobot(boolean isRobot) {
        this.isRobot = isRobot;
    }

    public boolean isRobot() {
        return isRobot;
    }

    public boolean isObstacle() {
        return isObstacle;
    }

    public void setObstacle(boolean isObstacle) {
        this.isObstacle = isObstacle;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }
}
