package ddr.it.unibo.utils;

import ddr.project.shared.QActorsConstants;
import it.unibo.qactors.akka.QActor;

public class movePlanUtilExplorer {

    private static String receiver = QActorsConstants.explorerPlayer;

    public static void move(QActor qa, String move, String duration) {
        movePlanUtil.move(qa, receiver, move, duration);
    }

    public static void moveNoMap(QActor qa, String move, String duration) {
        movePlanUtil.moveNoMap(qa, receiver, move, duration);
    }

    /*
     * ------------------------------------------------ TIMER
     * ------------------------------------------------
     */
    public static void startTimer(QActor qa) {
    	movePlanUtil.startTimer(qa);
    }

    public static void getDuration(QActor qa) {
    	movePlanUtil.getDuration(qa);
    }
}
