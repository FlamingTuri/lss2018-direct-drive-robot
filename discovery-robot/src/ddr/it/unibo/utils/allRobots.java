package ddr.it.unibo.utils;

import alice.tuprolog.NoSolutionException;
import alice.tuprolog.SolveInfo;
import ddr.project.led.Led;
import ddr.project.led.RaspberryLed;
import ddr.project.led.VirtualLed;
import it.unibo.qactors.akka.QActor;

public class allRobots {

    private static boolean on;
    private static boolean stop;
    private static Led led;
    private static RobotType type;

    public static void setUp(QActor qa, String robotType, String args) {
        try {
            qa.println("allRobots " + robotType + " setUp args=" + args);
            switch (robotType) {
            case "robotRealMbot":
                type = RobotType.robotRealMbot;
                ddr.it.unibo.robotMBot.basicRobotExecutor.setUp(qa, args);
                break;
            case "robotRealRaspOnly":
                type = RobotType.RASPBERRY;
                ddr.it.unibo.robotOnRaspOnly.basicRobotExecutor.setUp(qa);
                break;
            case "robotVirtual":
                type = RobotType.VIRTUAL;
                ddr.it.unibo.robotVirtual.basicRobotExecutor.setUp(qa, args);
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void doMove(QActor qa, String cmd) { // Args MUST be String
        try {
            SolveInfo sol = qa.solveGoal("robotType( T, _ )");
            String robotType = sol.getVarValue("T").toString();
            qa.println("allRobots " + robotType + " doMove cmd=" + cmd);
            switch (robotType) {
            case "robotRealMbot":
                ddr.it.unibo.robotMBot.basicRobotExecutor.doMove(qa, cmd);
                break;
            case "robotRealRaspOnly":
                ddr.it.unibo.robotOnRaspOnly.basicRobotExecutor.doMove(qa, cmd);
                break;
            case "robotVirtual":
                ddr.it.unibo.robotVirtual.basicRobotExecutor.doMove(qa, cmd);
                break;
            default:
                qa.println("Sorry, robot type unknown");
            }
        } catch (NoSolutionException e) {
            e.printStackTrace();
        }
    }

    public static void startBlinking(QActor qa) {
        stop = false;
        on = false;
        Thread blinker = new Thread() {
            @Override
            public void run() {
                while (!stop) {
                    on = !on;
                    led.switchOn();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    led.switchOff();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        qa.println("allRobots " + type + " start blinking");
        switch (type) {
        case robotRealMbot:
            // TODO
            break;
        case RASPBERRY:
            led = new RaspberryLed();
            break;
        case VIRTUAL:
            led = new VirtualLed();
            break;
        default:
            qa.println("Sorry, robot type unknown");
        }
        if (led != null) {
            blinker.start();
        }

    }

}
