## connettersi a raspberry via ethernet
1. `ssh pi@192.X.Y.Z`
2. password: ABC

## espandere il filesystem
1. sudo raspi-config
2. selezionare Advanced -> Expand Filesystem

## montare il raspberry come risorsa di rete su linux
1. creare una directory di mount con nome 'raspberry': `mkdir raspberry`
2. `sudo mount -t cifs //192.X.Y.Z/home_pi/ '/path/to/raspberry'`
3. inserire la password del raspberry

## spegnere raspberry gracefully
`sudo shutdown -r now`

## trasferire i file over ssh
`scp -r '/path/to/file' pi@192.X.Y.Z:/home/pi/Desktop`

l'opzione -r (recursive) serve per le trasferire le cartelle

## abilitare connessione schermo via hdmi
1. `sudo nano /boot/config.txt`
2. scommentare hdmi_safe=1

## disattivare/riattivare hotspot wifi
`sudo ifdown wlan0`
`sudo ifconfig wlan0 up`

### mostrare l'ip dei dispositivi connessi ad un dispositivo android 
lanciare `ip neigh`

### su linux ufw potrebbe bloccare le connessioni
lanciare `uwf disable`

---

## setup QActor eclipse

installare dal marketplace il plugin eclipse "xtext"

aggiungere alla cartella eclipse/dropins (trovare dove si è installato eclipse)
- it.unibo.xtext.qactor.ui_1.5.13.2.jar
- it.unibo.xtext.qactor_1.5.13.2.jar

eseguire `gradle build -b build_*ctxConsole.gradle wrapper`
eseguire `./gradlew build -b build_*ctxConsole.gradle eclipse`


---

## simulatore Soffritti

1. `cd to 'iss2018Lab/it.unibo.mbot2018/Soffritti'`
2. `./install.sh`
3. `./startServer.sh`
4. andare su localhost:8090
5. utilizzare wasd per muovere il robot

### pilotare il robot via web GUI utilizzando node

1. `cd 'iss2018Lab/it.unibo.mbot2018/nodecode/robot'`
2. node serverRobotCmd.js
2. andare su localhost:8080

### pilotare il robot con i QActor

1. `cd 'iss2018Lab/it.unibo.mbot2018qa'`
2. `gradle -b build_ctxDemoRobot.gradle eclipse`
3. effettuare il refresh del progetto eclipse
4. modificare iss2018Lab/it.unibo.mbot2018qa/src/demoRobot.qa commentando/scommentando il codice in base a quale implementazione si vuole utilizzare:
    - robotType( "robotVirtual", setuparg("localhost") ).
    - robotType( "robotRealMbot", setuparg("COM6") ).
    - robotType( "robotRealRaspOnly", setuparg("") ).
5. lanciare src-gen/MainCtxRobot.java come java application 
6. andare su localhost:8080
